#include <opencv2/opencv.hpp>
#include <vector>


__global__ void grayscale( unsigned char * in, unsigned char * out, std::size_t w, std::size_t h ) {
  extern __shared__ char tab1[];
  auto i = blockIdx.x * blockDim.x + threadIdx.x;
  auto j = blockIdx.y * blockDim.y + threadIdx.y;
  auto k = threadIdx.y * blockDim.x + threadIdx.x;
  auto d = j * w + i;
  if( i < w && j < h ) {
    tab1[ k ] = (
			 307 * in[ 3 * ( d ) ]
			 + 604 * in[ 3 * ( d ) + 1 ]
			 + 113 * in[  3 * ( d ) + 2 ]
			 ) / 1024;

  }
  __syncthreads();
  if(i<w&&j<h){
	out[ d ]=tab1[ k ];
  }

}

__global__ void smoothed( unsigned char * in, unsigned char * out, std::size_t w, std::size_t h ) {
  auto i = blockIdx.x * (blockDim.x-2) + threadIdx.x;
  auto j = blockIdx.y * (blockDim.y-2) + threadIdx.y;
  auto d = j * w + i;
  extern __shared__ unsigned char tab2[];

  auto li = threadIdx.x;
  auto lj = threadIdx.y;

  auto ww = blockDim.x;


  if( i < w && j < h ) {tab2[lj*ww+li]=in[d];}

  __syncthreads();

  if( li > 0 && li < (ww - 1) && lj > 0 && lj < (blockDim.y - 1) )
  {
    out[ d ] = (tab2[ ( lj - 1 ) * ww + li - 1 ] + tab2[ ( lj - 1 ) * ww + li     ] + tab2[ ( lj - 1 ) * ww + li + 1 ]
             +  tab2[ ( lj     ) * ww + li - 1 ] + tab2[ ( lj     ) * ww + li     ] + tab2[ ( lj     ) * ww + li + 1 ]
             +  tab2[ ( lj + 1 ) * ww + li - 1 ] + tab2[ ( lj + 1 ) * ww + li     ] + tab2[ ( lj + 1 ) * ww + li + 1 ])/9;


  }
}

__global__ void detail( unsigned char * in, unsigned char * in2, unsigned char * out, std::size_t w, std::size_t h ) {
  auto i = blockIdx.x * blockDim.x + threadIdx.x;
  auto j = blockIdx.y * blockDim.y + threadIdx.y;
  if( i > 1 && i < (w - 1) && j > 1 && j < (h - 1) )
  {

    out[ j * w + i ] = in[ j * w + i ] - in2[ j * w + i ];

  }
}

__global__ void sharpen( unsigned char * in, unsigned char * in2, unsigned char * out, std::size_t w, std::size_t h ) {
  auto i = blockIdx.x * blockDim.x + threadIdx.x;
  auto j = blockIdx.y * blockDim.y + threadIdx.y;
  if( i > 1 && i < (w - 1) && j > 1 && j < (h - 1) )
  {

        out[ j * w + i ] = in[ j * w + i ] + in2[ j * w + i ];

  }
}


int main( int argc, char **args )
{

  using namespace std;
  // we need 4 arguments: input output blockDim.x blockDimx.y
  if ( argc != 5 ) {
    cout << "Invalid number of argument " << endl;
    cout << "Usage: <blockDim.x : int> <blockDim.y : int> <input : str> <ouput : str>" << endl;
    exit( 1 );
  }
  int block_dim_x = atoi( args[ 1 ] );
  int block_dim_y = atoi( args[ 2 ] );
  char *input_filename = args[ 3 ];
  char *ouput_filename = args[ 4 ];

  cv::Mat m_in = cv::imread( input_filename, cv::IMREAD_UNCHANGED );
  auto rgb = m_in.data;
  auto rows = m_in.rows;
  auto cols = m_in.cols;
  std::vector< unsigned char > g( rows * cols );
  cv::Mat m_out( rows, cols, CV_8UC1, g.data() );
  unsigned char * rgb_d;
  unsigned char * g_d;
  unsigned char * g_e;
  unsigned char * g_f;
  unsigned char * g_g;
  cudaMalloc( &rgb_d, 3 * rows * cols );
  cudaMalloc( &g_d, rows * cols );
  cudaMalloc( &g_e, rows * cols );
  cudaMalloc( &g_f, rows * cols );
  cudaMalloc( &g_g, rows * cols );
  cudaMemcpy( rgb_d, rgb, 3 * rows * cols, cudaMemcpyHostToDevice );

  // prepares cuda events to compute elapsed time for filter computation
  cudaEvent_t start, stop;
  cudaEventCreate( &start );
  cudaEventCreate( &stop );
  cudaEventRecord( start );
  
  dim3 t( block_dim_x, block_dim_y );
  dim3 b( ( cols - 1) / t.x + 1 , ( rows - 1 ) / t.y + 1 );
  grayscale<<< b, t, (t.x*t.y)*sizeof(char) >>>( rgb_d, g_d, cols, rows );
  dim3 c( ( cols - 1) / (t.x-2) + 1 , ( rows - 1 ) / (t.y-2) + 1 );
  smoothed<<< c, t, (t.x*t.y)*sizeof(char) >>>( g_d, g_e, cols, rows );
  detail<<< b, t >>>( g_d, g_e, g_f, cols, rows );
  sharpen<<< b, t >>>( g_d, g_f, g_g, cols, rows );

  // get current time to compute elapsed time
  cudaEventRecord( stop );
  cudaEventSynchronize( stop );

  float elapsedTime;
  cudaEventElapsedTime( & elapsedTime, start, stop );
  // display elapsed time
  cout << "Total GPU computation time: " << elapsedTime << "ms with grid dimensions " << t.x << " " << t.y <<endl;;


  cudaDeviceSynchronize();
  auto err = cudaGetLastError();
  if( err != cudaSuccess )
  {
    std::cout <<"Cuda error "<< cudaGetErrorString( err );
  }

  cudaMemcpy( g.data(), g_g, rows * cols, cudaMemcpyDeviceToHost );
  cv::imwrite( ouput_filename, m_out );
  cudaFree( rgb_d);
  cudaFree( g_d);
  cudaFree( g_e);
  cudaFree( g_f);
  cudaFree( g_g);
  return 0;
}
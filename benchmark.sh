project_base=$(pwd)
bench_file="$project_base/bench/best_bench.txt"
input_file="$project_base/res/big_in.jpg" # <-------- Modify input image here 
output_file="$project_base/res/out.jpg"

echo "Project base: $project_base"
echo "Bench file location: $bench_file"

# create (or clear) best score file
if ([ -e "$bench_file" ] && [ -f "$bench_file" ]); then
    rm "$bench_file"
fi
touch "$bench_file"


# compiles all sharpen filters and put binaries in build folder
cd src/cpu
echo "Compiling CPU version"
make
cd ..
for version in $( seq 1 4 ); do
    echo "Compiling version $version"
    cd sharpen-v$version
    make
    cd ..
done


# run all sharpen filters
cd "$project_base/build"


# first, execute cpu version
echo "Executing CPU version"
echo "========== CPU ============"  >> "$bench_file"
./sharpen-cpu "$input_file" "$output_file" >> "$bench_file"


if ([ -e tmp.txt ] && [ -f tmp.txt ]); then
    rm tmp.txt
fi


# executes all versions between 1 and 3 (with no streams)
for version in $( seq 1 3 ); do
    echo "Executing version $version..."
    # executes all block configurations 
    touch tmp.txt
    for x in $( seq 8 2 32 ); do
        for y in $( seq 8 2 32 ); do
            ./sharpen-v$version $x $y "$input_file" "$output_file" >> tmp.txt
        done
    done
    
    # put the best result in bench_file
    echo "========== Version $version ==========" >> "$bench_file"
    cat tmp.txt | sort -k 5 | head -1 >> "$bench_file"
    rm tmp.txt
done


    
# execute version 4 (with streams)
echo "Executing version 4..."
touch tmp.txt
for nstreams in $( seq 2 2 8 ); do
    for x in $( seq 8 2 32 ); do
        for y in $( seq 8 2 32 ); do
            ./sharpen-v4 $x $y $nstreams "$input_file" "$output_file" >> tmp.txt
        done
    done
done

# put the best result in bench_file
echo "========== Version 4 ==========" >> "$bench_file"
cat tmp.txt | sort -k 5 | head -1 >> "$bench_file"
rm tmp.txt


